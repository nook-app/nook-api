<?php

namespace App\DataFixtures;

use App\Entity\Demand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DemandFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $demand = new Demand();
        $demand
            ->setTitle('Faire les courses')
            ->setDescription("J'ai besoin d'aide pour faire mes courses je suis seule et je n'ai pas de voiture")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_ACHAT))
        ;

        $manager->persist($demand);

        $demand = new Demand();
        $demand
            ->setTitle('Achat de médicament')
            ->setDescription("J'ai un pied cassé, pourriez-vous m'aider svp ?")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_ACHAT))
        ;

        $manager->persist($demand);

        $demand = new Demand();
        $demand
            ->setTitle('Problème de plomberie')
            ->setDescription("J'ai une fuite d'eau dans ma cuisine ! et le tuyau tout dur... J'ai besoin d'aide svp ?")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DEPANAGE))
        ;

        $manager->persist($demand);

        $demand = new Demand();
        $demand
            ->setTitle('Ménage dans ma résidence secondaire !')
            ->setDescription("Ne pouvant me déplacer, je cherche un femme de ménage pour l'été ?")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DOMESTIQUE))
        ;

        $manager->persist($demand);

        $demand = new Demand();
        $demand
            ->setTitle("J'ai un soucis d'écoulement !")
            ->setDescription('Je suis une femme seule qui a besoin de stopper la fuite svp !')
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DEPANAGE))
        ;

        $manager->persist($demand);

        $demand = new Demand();
        $demand
            ->setTitle('Livraison de colis')
            ->setDescription("Je ne peux pas allez chercher mon colis, à la poste du kikimou, pourriez-vous m'aider ?")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DOMESTIQUE))
        ;

        $manager->persist($demand);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
        ];
    }
}
