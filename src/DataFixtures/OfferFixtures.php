<?php

namespace App\DataFixtures;

use App\Entity\Offer;
use App\DataFixtures\CategoryFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class OfferFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $offer = new Offer();
        $offer
            ->setTitle("Aide aux courses")
            ->setDescription("Je suis un homme fort, je soulève bien !")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_ACHAT))
        ;

        $manager->persist($offer);

        $offer = new Offer();
        $offer
            ->setTitle("Vente stock de préservatif")
            ->setDescription("Je ne peux pas sortir de chez moi, je ne me sert que de la Marie !")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_ACHAT))
        ;

        $manager->persist($offer);

        $offer = new Offer();
        $offer
            ->setTitle("Faire la vaiselle")
            ->setDescription("Disponible pour faire la vaiselle d'un homme seul, et plus si affinité...")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DOMESTIQUE))
        ;

        $manager->persist($offer);

        $offer = new Offer();
        $offer
            ->setTitle("Offre blague lourde")
            ->setDescription("Je fais des live sur youtube pour passer le temps, c'est gratuit venez !!")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DEPANAGE))
        ;

        $manager->persist($offer);

        $offer = new Offer();
        $offer
            ->setTitle("Décorateur")
            ->setDescription("Homme bricoleur, cherche a refaire la déco d'intérieur chez personne seule, vous n'allez rien reconnaître !")
            ->addCategory($this->getReference(CategoryFixtures::CATEGORY_DOMESTIQUE))
        ;

        $manager->persist($offer);



        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
