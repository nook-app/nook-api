<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public const CATEGORY_ACHAT = 'category-achat';
    public const CATEGORY_DEPANAGE = 'category-depanage';
    public const CATEGORY_DOMESTIQUE = 'category-domestique';

    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('Achats');
        $manager->persist($category);

        $this->addReference(self::CATEGORY_ACHAT, $category);

        $category = new Category();
        $category->setName('Dépanage');
        $manager->persist($category);

        $this->addReference(self::CATEGORY_DEPANAGE, $category);

        $category = new Category();
        $category->setName('Domestique');
        $manager->persist($category);

        $this->addReference(self::CATEGORY_DOMESTIQUE, $category);

        $manager->flush();
    }
}
