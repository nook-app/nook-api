<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends AbstractController
{
    protected $repo;
    protected $em;

    /**
     * Constructor.
     */
    public function __construct(CategoryRepository $repo, EntityManagerInterface $em)
    {
        $this->repo = $repo;
        $this->em = $em;
    }

    /**
     * Get all categorys.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Get("/category")
     */
    public function getAll()
    {
        return $this->repo->findAll();
    }

    /**
     * Get a single category.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Get("/category/{slug}")
     */
    public function getOne(Category $category)
    {
        return $category;
    }

    /**
     * Post a new category.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"category"}, statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/category")
     */
    public function post(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $data = $request->request->all();
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($category);
            $this->em->flush();

            return $category;
        }

        return $form;
    }

    /**
     * Update a category.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Patch("/category/{slug}")
     */
    public function patch(Category $category, Request $request)
    {
        $form = $this->createForm(CategoryType::class, $category);

        $data = $request->request->all();
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $category;
        }

        return $form;
    }

    /**
     * Delete a category.
     *
     * @return JsonResponse
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/category/{slug}")
     */
    public function delete(Category $category)
    {
        $this->em->remove($category);
        $this->em->flush();

        return View::create(['message' => 'Category deleted'], Response::HTTP_NO_CONTENT);
    }
}
