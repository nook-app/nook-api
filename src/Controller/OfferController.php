<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OfferController extends AbstractController
{
    protected $repo;
    protected $em;

    /**
     * Constructor.
     */
    public function __construct(OfferRepository $repo, EntityManagerInterface $em)
    {
        $this->repo = $repo;
        $this->em = $em;
    }

    /**
     * Get all offers.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"offer"})
     * @Rest\Get("/offer")
     */
    public function getAll()
    {
        return $this->repo->findAll();
    }

    /**
     * Get a single offer.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"offer"})
     * @Rest\Get("/offer/{slug}")
     */
    public function getOne(Offer $offer)
    {
        return $offer;
    }

    /**
     * Post a new offer.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"offer"}, statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/offer")
     */
    public function post(Request $request)
    {
        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);

        $data = $request->request->all();
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($offer);
            $this->em->flush();

            return $offer;
        }

        return $form;
    }

    /**
     * Update a offer.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"offer"})
     * @Rest\Patch("/offer/{slug}")
     */
    public function patch(Offer $offer, Request $request)
    {
        $form = $this->createForm(OfferType::class, $offer);

        $data = $request->request->all();
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $offer;
        }

        return $form;
    }

    /**
     * Delete a offer.
     *
     * @return JsonResponse
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/offer/{slug}")
     */
    public function delete(Offer $offer)
    {
        $this->em->remove($offer);
        $this->em->flush();

        return View::create(['message' => 'Offer deleted'], Response::HTTP_NO_CONTENT);
    }
}
