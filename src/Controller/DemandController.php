<?php

namespace App\Controller;

use App\Entity\Demand;
use App\Form\DemandType;
use App\Repository\DemandRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DemandController extends AbstractController
{
    protected $repo;
    protected $em;

    /**
     * Constructor.
     */
    public function __construct(DemandRepository $repo, EntityManagerInterface $em)
    {
        $this->repo = $repo;
        $this->em = $em;
    }

    /**
     * Get all demands.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"demand"})
     * @Rest\Get("/demand")
     */
    public function getAll()
    {
        return $this->repo->findAll();
    }

    /**
     * Get a single demand.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"demand"})
     * @Rest\Get("/demand/{slug}")
     */
    public function getOne(Demand $demand)
    {
        return $demand;
    }

    /**
     * Post a new demand.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"demand"}, statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/demand")
     */
    public function post(Request $request)
    {
        $demand = new Demand();
        $form = $this->createForm(DemandType::class, $demand);

        $data = $request->request->all();
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($demand);
            $this->em->flush();

            return $demand;
        }

        return $form;
    }

    /**
     * Update a demand.
     *
     * @return JsonResponse
     *
     * @Rest\View(serializerGroups={"demand"})
     * @Rest\Patch("/demand/{slug}")
     */
    public function patch(Demand $demand, Request $request)
    {
        $form = $this->createForm(DemandType::class, $demand);

        $data = $request->request->all();
        $form->submit($data, false);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $demand;
        }

        return $form;
    }

    /**
     * Delete a demand.
     *
     * @return JsonResponse
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/demand/{slug}")
     */
    public function delete(Demand $demand)
    {
        $this->em->remove($demand);
        $this->em->flush();

        return View::create(['message' => 'Demand deleted'], Response::HTTP_NO_CONTENT);
    }
}
